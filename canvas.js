let canvas = document.getElementById("myCanvas");
let ctx = canvas.getContext("2d");
let eraser = document.getElementById("eraser");
let brush = document.getElementById("brush");
let rect = document.getElementById("rect");
let circle = document.getElementById("circle");
let tri = document.getElementById("triangle");
let rainbow = document.getElementById("rainbow");
let reSetCanvas = document.getElementById("clear");
let aColorBtn = document.getElementsByClassName("color-item");
let save = document.getElementById("save");
let undo = document.getElementById("undo");
let redo = document.getElementById("redo");
let commit = document.getElementById("commit");
let word = document.getElementById("word");
let fsize=document.getElementById("fsize");
let fstyle=document.getElementById("fstyle");
let image=document.getElementById("image");
let range = document.getElementById("range");
let clear = false;
let activeColor = 'black';
let lWidth = 4;
let table= document.getElementById("table");
let cbox=document.getElementById("cbox");
let board=document.getElementById("board");
let painting = false;
let lastPoint = {x: undefined, y: undefined};
let newPoint = {x: undefined, y: undefined};

let historyDeta = [];
let undoHistory= [];
let dynamic = [];
let shape = 0;                         //0为线条 1矩形 2圆形 3 三角形 

    SetSize();
    setCanvasBg('white');
    listenToUser(canvas);
    getColor();


function SetSize(){
    cbox.style.width=canvas.width;
    cbox.style.height=canvas.height;
}

function setCanvasBg(color) {
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = "black";
}

function drawCircle(x, y, radius) {                           //画点
    ctx.save();
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, Math.PI * 2);
    ctx.fill();
    if (clear) {
        ctx.clip();
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.restore();
    }
}

function drawLine(x1, y1, x2, y2,clear) {                    //鼠标移动速度较快时画线
    ctx.lineWidth = lWidth;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    if (clear) {
        ctx.save();
        ctx.globalCompositeOperation = "destination-out";
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.stroke();
        ctx.closePath();
        ctx.clip();
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.restore();
    }else{
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.stroke();
        ctx.closePath();
    }
}

brush.onclick = function () {
    clear = false;
    this.classList.add("active");
    shape=0;
    eraser.classList.remove("active");
    rect.classList.remove("active");
    circle.classList.remove("active");
    tri.classList.remove("active");
    commit.classList.remove("active");
    canvas.style.cursor="url('img/favicon.ico'),crosshair";
}
eraser.onclick = function () {
    clear = true;
    shape=0;
    this.classList.add("active");
    brush.classList.remove("active");
    rect.classList.remove("active");
    circle.classList.remove("active");
    tri.classList.remove("active");
    commit.classList.remove("active");
    canvas.style.cursor="url('img/erasericon.ico'),crosshair";
}

rect.onclick = function () {
    clear = false;
    this.classList.add("active");
    shape=1;
    console.log(shape);
    eraser.classList.remove("active");
    circle.classList.remove("active");
    brush.classList.remove("active");
    tri.classList.remove("active");
    commit.classList.remove("active");
    canvas.style.cursor="crosshair";
}

circle.onclick=function(){
    clear = false;
    this.classList.add("active");
    shape=2;
    console.log(shape);
    eraser.classList.remove("active");
    brush.classList.remove("active");
    rect.classList.remove("active");
    tri.classList.remove("active");
    commit.classList.remove("active");
    canvas.style.cursor="crosshair";
}
tri.onclick=function(){
    clear = false;
    this.classList.add("active");
    shape=3;
    console.log(shape);
    eraser.classList.remove("active");
    brush.classList.remove("active");
    rect.classList.remove("active");
    circle.classList.remove("active");
    commit.classList.remove("active");
    canvas.style.cursor="crosshair";
}
commit.onclick=function(){
    clear = false;
    this.classList.add("active");
    shape=4;
    console.log(shape);
    eraser.classList.remove("active");
    brush.classList.remove("active");
    rect.classList.remove("active");
    circle.classList.remove("active");
    tri.classList.remove("active");
    ctx.fillStyle =getColor(); // 文本颜色
    ctx.textAlign = 'center'; // 文本对齐方式 
    canvas.style.cursor="crosshair";
    let index1=fsize.selectedIndex ; 
    let fs=fsize.options[index1].value;         // selectedIndex代表的是你所选中项的index
    let index2=fstyle.selectedIndex ;
    let typeface=fstyle.options[index2].value;   
   
    switch(fs)
    {
        case "16":{
            switch(typeface){
            case "1":{ ctx.font="16px Arial";break;}
            case "2":{ ctx.font="16px Times New Roman";break;}
            case "3":{ ctx.font="16px KaiTi";break;}
            case "4":{ ctx.font="16px PMingLiU";break;}
            }
            console.logt("16",typeface);
            break;
        }
        case "20":{
            switch(typeface){
                case "1":{ ctx.font="20px Arial";break;}
                case "2":{ ctx.font="20px Times New Roman";break;}
                case "3":{ ctx.font="20px KaiTi";break;}
                case "4":{ ctx.font="20px PMingLiU";break;}
                }
                console.logt("20",typeface);
                break;
        }
        case "24":{
            switch(typeface){
                case "1":{ ctx.font="24px Arial";break;}
                case "2":{ ctx.font="24px Times New Roman";break;}
                case "3":{ ctx.font="24px KaiTi";break;}
                case "4":{ ctx.font="24px PMingLiU";break;}
                }
                console.logt("24",typeface);
                break;
        }
        case "28":{
            switch(typeface){
                case "1":{ ctx.font="28px Arial";break;}
                case "2":{ ctx.font="28px Times New Roman";break;}
                case "3":{ ctx.font="28px KaiTi";break;}
                case "4":{ ctx.font="24px PMingLiU";break;}
                }
                console.logt("28",typeface);
                break;
        }
        case "32":{
            switch(typeface){
                case "1":{ ctx.font="32px Arial";break;}
                case "2":{ ctx.font="32px Times New Roman";break;}
                case "3":{ ctx.font="32px KaiTi";break;}
                case "4":{ ctx.font="32px PMingLiU";break;}
                }
                console.logt("32",fstyle);
                break;
        }
        case "36":{
            switch(fstyle){
                case "1":{ ctx.font="36px Arial";break;}
                case "2":{ ctx.font="36px Times New Roman";break;}
                case "3":{ ctx.font="36px KaiTi";break;}
                case "4":{ ctx.font="36px PMingLiU";break;}    
            }
                console.logt("36",typeface);
                break;
        }
        case "40":{
            switch(typeface){
                case "1":{ ctx.font="40px Arial";break;}
                case "2":{ ctx.font="40px Times New Roman";break;}
                case "3":{ ctx.font="40px KaiTi";break;}
                case "4":{ ctx.font="40px PMingLiU";break;}
                }
                console.logt("40",typeface);
                break;
        }
        case "44":{
            switch(typeface){
                case "1":{ ctx.font="44px Arial";break;}
                case "2":{ ctx.font="44px Times New Roman";break;}
                case "3":{ ctx.font="44px KaiTi";break;}
                case "4":{ ctx.font="44px PMingLiU";break;}
                }
                console.logt("44",typeface);
                break;
        }
    };
    
}

reSetCanvas.onclick = function () {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    setCanvasBg('white');
};

save.onclick = function () {
    let imgUrl = canvas.toDataURL("image/png");
    let saveA = document.createElement("a");
    document.body.appendChild(saveA);
    saveA.href = imgUrl;
    saveA.download = "mypaint" + (new Date).getTime();
    saveA.target = "_blank";
    saveA.click();
    canvas.style.cursor="default";
};

function getColor(){
    for (let i = 0; i < aColorBtn.length; i++) {
        aColorBtn[i].onclick = function () {
            for (let i = 0; i < aColorBtn.length; i++) {
                aColorBtn[i].classList.remove("active");
                this.classList.add("active");
                activeColor = this.style.backgroundColor;
                ctx.fillStyle = activeColor;
                ctx.strokeStyle = activeColor;
            }
        }
    }
}

function saveData (data) {
    (historyDeta.length === 20) && (historyDeta.shift());// 上限为储存20步
    historyDeta.push(data);
}

redo.onclick = function(){
    if(undoHistory.length < 1) return false;
    this.firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height);
    historyDeta.push(this.firstDot);
    ctx.putImageData(undoHistory.pop(), 0, 0);
    canvas.style.cursor="default";
}

undo.onclick = function(){
    if(historyDeta.length < 1) return false;
    this.firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height);//在这里储存绘图表面
    undoHistory.push(this.firstDot);
    ctx.putImageData(historyDeta[historyDeta.length - 1], 0, 0);
    historyDeta.pop()
    canvas.style.cursor="default";
};

function listenToUser(canvas){
    canvas.onmousedown = function (e) {                   //鼠标按下事件
        console.log("brushonmousedown");
        this.firstDot = ctx.getImageData(0, 0, canvas.width, canvas.height);//在这里储存绘图表面
        saveData(this.firstDot);
        painting = true;
        let x = e.clientX-canvas.offsetLeft-table.offsetLeft;
        let y = e.clientY-canvas.offsetTop-table.offsetTop;
        lastPoint = {"x": x, "y": y};
        console.log(x,y);                                   //debug
        ctx.save();
        drawCircle(x, y, 0);
    };
    canvas.onmousemove = function (e) {                   //鼠标移动事件
        if (painting) {
            let x = e.clientX-canvas.offsetLeft-table.offsetLeft;
            let y = e.clientY-canvas.offsetTop-table.offsetTop;
            newPoint = {"x": x, "y": y};
            switch (shape) {
                case 0:{
                    console.log("line");
                    drawLine(lastPoint.x, lastPoint.y, newPoint.x, newPoint.y,clear);
                    lastPoint = newPoint;
                    break;
                }
                case 1:{
                    ctx.putImageData(historyDeta[historyDeta.length - 1], 0, 0);
                    ctx.fillRect(lastPoint.x, lastPoint.y, newPoint.x-lastPoint.x, newPoint.y-lastPoint.y);
                    
                    console.log("rectangle");
                    break;
                }
                case 2:{
                    console.log(x,y);                     
                    let xdiff=newPoint.x-lastPoint.x;
                    let ydiff=newPoint.y-lastPoint.y;
                    let r=Math.pow((xdiff * xdiff + ydiff * ydiff), 0.5);
                    console.log(r);
                    ctx.putImageData(historyDeta[historyDeta.length - 1], 0, 0);
                    ctx.arc(lastPoint.x,lastPoint.y,r,0, Math.PI * 2);
                    ctx.fill();
                    break;
                }
                case 3:{
                    ctx.putImageData(historyDeta[historyDeta.length - 1],0,0);
                    break;
                }
                case 4:{
                    ctx.putImageData(historyDeta[historyDeta.length - 1], 0, 0);
                    ctx.fillText(word.value, lastPoint.x, lastPoint.y, newPoint.x-lastPoint.x);
                    break;
                }
                case 5:{
                    ctx.putImageData(historyDeta[historyDeta.length - 1], 0, 0);
                    ctx.drawImage(image, lastPoint.x, lastPoint.y,newPoint.x-lastPoint.x,newPoint.y-lastPoint.y);
                    break;
                }
                  
            }     
        }
    };
    canvas.onmouseup = function () {                      //松开鼠标事件
        painting = false;
        console.log("brushonmouseup");
        if(shape==3)
        {
            ctx.putImageData(historyDeta[historyDeta.length - 1],0,0);
            let height=newPoint.y-lastPoint.y;
            let hfwidth=newPoint.x-lastPoint.x;
            ctx.moveTo(lastPoint.x,lastPoint.y);
            ctx.lineTo(newPoint.x,newPoint.y);
            ctx.lineTo(newPoint.x-2*hfwidth,newPoint.y);
            ctx.fill();
            ctx.closePath();
        }
        
    };
    canvas.onmouseout = function () {                     //鼠标移出画板事件
        painting = false;
    }      
}
                                                           
range.onchange = function(){
    lWidth = this.value;
};

rainbow.onclick = function(){
    var canvasGradient = ctx.createLinearGradient(0,0,canvas.width,canvas.height); 
    canvasGradient.addColorStop(0, "white"); 
    canvasGradient.addColorStop(0.2, "yellow"); //在offset为0.2的位置(即起点位置)添加一个渐变
    canvasGradient.addColorStop(0.4, "red"); 
    canvasGradient.addColorStop(0.6, "purple");
    canvasGradient.addColorStop(0.8, "blue");
    canvasGradient.addColorStop(0.9, "green");
    canvasGradient.addColorStop(0.75, "chocolate");
    ctx.strokeStyle = canvasGradient; //将strokeStyle的属性值设为该CanvasGradient对象

}

$("#img_input").on("change", function(e) {
    file = e.target.files[0]; //获取图片资源
    // 只选择图片文件
    if (!file.type.match('image.*')) {
      return false;
    }
    var reader = new FileReader();
    reader.readAsDataURL(file); // 读取文件
    // 渲染文件
    reader.onload = function(arg) {
      var img = '<img class="preview"  src="' + arg.target.result + '" alt="preview"/>';
      $(".preview_box").empty().append(img);
      image.src=arg.target.result;
    };
    shape=5;
    canvas.style.cursor="crosshair";
  });





