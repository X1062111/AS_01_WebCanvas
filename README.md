# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

功能描述：
1.颜色选择器：共七个颜色可选择，可用于画笔颜色，文本颜色，矩形圆形三角形的填充
2.画笔与橡皮： 均可调节粗细（大小），可完成涂鸦与擦除。
3.笔刷粗细调节器：右下方，可调节笔刷与橡皮的大小
4.文本输入框：在输入框内输入文字后，选择字号和字体，点击确定，然后在canvas内拖动鼠标，完成文本的放置；重新输入文字或更改字号字体后，都需要重新确认。
5.鼠标指针：点击画笔或橡皮，鼠标进入canvas后，cursor变成相应图案；三个几何形状及插入文本和图片时的cursor为十字指针；其他按钮无特殊cursor；
6.点击清空键，重置画布，清除后可通过undo来撤销。
7.矩形和圆形可显示绘画过程，三角形在松开鼠标左键时完成绘画；
8.Un/Redo：可以撤销并返回，但最多只能撤销二十步（存储的都是图片，避免down机）
9.图片上传与贴置：浏览并选择本地图片上传后，在canvas内拖动鼠标，选择放置的区域和图片大小比例；
                使用了jq所以需用http协议；
                选择本地图片后，在选择框下方可进行图片预览；
10.下载：可将绘图结果下载并保存，默认文件名为mycanvas+日期时间。

